import 'package:flutter/material.dart';
import 'package:mobilemix/types.dart';

class Menu extends StatelessWidget {
  final Function switchTheme;

  Menu({@required this.switchTheme});

  Future<void> _showAbout(BuildContext outerContext) => showDialog<void>(
      context: outerContext,
      barrierDismissible: false,
      builder: (BuildContext innerContext) => AlertDialog(
              title: Text('About'),
              content: Text('A cool practice app I made.'),
              actions: <Widget>[
                FlatButton(
                    child: Text('Neat'),
                    onPressed: () => Navigator.of(innerContext).pop())
              ]));

  @override
  Widget build(BuildContext context) => PopupMenuButton<MenuAction>(
      elevation: 3.2,
      onSelected: (MenuAction action) {
        switch (action) {
          case MenuAction.about:
            _showAbout(context);
            break;
          case MenuAction.switchTheme:
            switchTheme();
            break;
          default:
            break;
        }
      },
      itemBuilder: (BuildContext innerContext) => <PopupMenuEntry<MenuAction>>[
            const PopupMenuItem<MenuAction>(
                child: Text('Switch Theme'), value: MenuAction.switchTheme),
            PopupMenuDivider(),
            const PopupMenuItem<MenuAction>(
                child: Text('About'), value: MenuAction.about)
          ]);
}
