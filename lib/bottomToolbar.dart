import 'package:flutter/material.dart';

class BottomToolbar extends StatelessWidget {
  final Function mixRecipe;
  final Function clearFlavors;

  BottomToolbar({@required this.mixRecipe, @required this.clearFlavors});

  @override
  Widget build(BuildContext context) => BottomAppBar(
      shape: CircularNotchedRectangle(),
      notchMargin: 4.0,
      child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            IconButton(
                icon: Icon(Icons.bubble_chart),
                onPressed: () => mixRecipe(context)),
            IconButton(icon: Icon(Icons.clear_all), onPressed: clearFlavors)
          ]));
}
