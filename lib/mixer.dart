import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobilemix/types.dart';

bool isNumeric(String val) => double.parse(val) != null;

class Mixer extends StatelessWidget {
  final List<Flavor> flavors;
  final Function(Flavor, String) updateFlavorName;
  final Function(Flavor, double) updateFlavorPercent;
  final Function(BuildContext, Flavor) removeFlavor;

  Mixer(
      {@required this.flavors,
      @required this.removeFlavor,
      @required this.updateFlavorName,
      @required this.updateFlavorPercent});

  Widget flavorInputs(Flavor flavor) =>
      Row(mainAxisAlignment: MainAxisAlignment.start, children: [
        Expanded(
            flex: 10,
            child: Padding(
                padding: EdgeInsets.only(right: 10.0),
                child: TextField(
                    onChanged: (String newValue) =>
                        updateFlavorName(flavor, newValue),
                    onSubmitted: (String newValue) {
                      debugPrint('Submitted: $newValue');
                    },
                    decoration: InputDecoration(hintText: 'Flavor Name')))),
        Expanded(
            flex: 2,
            child: TextField(
              onChanged: (String newValue) {
                double v = double.tryParse(newValue);
                if (v != null) {
                  updateFlavorPercent(flavor, v);
                }
              },
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              decoration: InputDecoration(hintText: '%'),
            ))
      ]);

  /* Idea for swipe to remove: remove flavor first then confirm. If user says
   * no, reinsert flavor into its original position with an animation.
   */
  @override
  Widget build(BuildContext context) => ListView(
      children: flavors
          .map((Flavor flavor) => Dismissible(
              key: Key('Flavor${flavor.id}'),
              onDismissed: (DismissDirection dir) =>
                  removeFlavor(context, flavor),
              child: Container(
                  padding: EdgeInsets.all(20),
                  child: Form(child: flavorInputs(flavor)))))
          .toList());
}
