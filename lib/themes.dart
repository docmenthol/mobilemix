import 'package:flutter/material.dart';

class Themes {
  static final dark = ThemeData(
      brightness: Brightness.dark,
      primaryColor: Colors.teal[800],
      accentColor: Colors.pink[800],
      textTheme: TextTheme(button: TextStyle(color: Colors.white)));

  static final light = ThemeData(
      brightness: Brightness.light,
      primaryColor: Colors.teal[800],
      accentColor: Colors.pink[800]);
}
