import 'package:flutter/material.dart';
import 'package:mobilemix/types.dart';

class RecipeDrawer extends StatefulWidget {
  final List<Recipe> recipes;
  final Function(BuildContext, Recipe) loadRecipe;
  final Function(String) saveRecipe;

  RecipeDrawer(
      {@required this.recipes,
      @required this.loadRecipe,
      @required this.saveRecipe});

  @override
  RecipeDrawerState createState() => RecipeDrawerState(
      recipes: recipes, loadRecipe: loadRecipe, saveRecipe: saveRecipe);
}

class RecipeDrawerState extends State<RecipeDrawer> {
  final _headerFont = const TextStyle(fontSize: 24.0, color: Colors.white);
  final List<Recipe> recipes;
  final Function(BuildContext, Recipe) loadRecipe;
  final Function(String) saveRecipe;

  String recipeName;

  RecipeDrawerState(
      {@required this.recipes,
      @required this.loadRecipe,
      @required this.saveRecipe});

  Future<void> saveRecipeDialog(BuildContext outerContext) => showDialog<void>(
      context: outerContext,
      builder: (BuildContext innerContext) => AlertDialog(
              title: Text('Save Recipe'),
              content: TextField(
                  controller: TextEditingController(text: recipeName),
                  decoration: InputDecoration(hintText: 'Recipe Name'),
                  onChanged: (String value) {
                    debugPrint(value);
                    setState(() {
                      recipeName = value;
                    });
                  }),
              actions: <Widget>[
                FlatButton(
                    child: Text('Cancel'),
                    onPressed: () => Navigator.pop(context, false)),
                FlatButton(
                    child: Text('Save'),
                    onPressed: () {
                      saveRecipe(recipeName);
                      Navigator.pop(context, false);
                    })
              ]));

  Widget _buildRecipeRow(BuildContext context, Recipe recipe) => ListTile(
      title: Text(recipe.name), onTap: () => loadRecipe(context, recipe));

  @override
  Widget build(BuildContext context) {
    List<Widget> drawerItems = recipes
        .expand((recipe) => [_buildRecipeRow(context, recipe), Divider()])
        .toList();
    drawerItems.insert(
        0,
        DrawerHeader(
            child: Align(
                alignment: AlignmentDirectional.bottomStart,
                child: Text('Recipes', style: _headerFont)),
            decoration: BoxDecoration(color: Theme.of(context).primaryColor)));
    drawerItems.add(ListTile(
        title: Center(child: Icon(Icons.add_circle)),
        onTap: () => saveRecipeDialog(context)));
    return ListView(children: drawerItems);
  }
}
