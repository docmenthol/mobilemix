import 'package:flutter/material.dart';
import 'package:mobilemix/bottomToolbar.dart';
import 'package:mobilemix/menu.dart';
import 'package:mobilemix/mixer.dart';
import 'package:mobilemix/recipeDrawer.dart';
import 'package:mobilemix/themes.dart';
import 'package:mobilemix/types.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'dart:developer';

void main() {
  runApp(App());
}

class App extends StatefulWidget {
  @override
  AppState createState() => AppState();
}

class AppState extends State<App> {
  List<Flavor> flavors = [];

  List<Recipe> recipes = [
    Recipe(flavors: [
      Flavor(name: 'Strawberry', percent: 3.0),
      Flavor(name: 'Lemonade', percent: 7.0)
    ], name: 'Strawberry Lemonade', id: 0),
    Recipe(flavors: [
      Flavor(name: 'Raspberry', percent: 2.0),
      Flavor(name: 'Lemonade', percent: 7.0)
    ], name: 'Raspberry Lemonade', id: 1)
  ];

  // These keys are type-checked by the _updateParameter function.
  Map<String, double> parameters = Map.fromEntries([
    MapEntry('nic', 100.0),
    MapEntry('target', 3.0),
    MapEntry('amount', 60.0),
    MapEntry('pg', 0.0),
    MapEntry('vg', 100.0)
  ]);

  AppTheme appTheme = AppTheme.Dark;

  @override
  void initState() {
    super.initState();
    loadPrefs();
  }

  void loadPrefs() => SharedPreferences.getInstance()
      .then((prefs) => prefs.getInt('appTheme'))
      .then((int themeIndex) => setState(() {
            appTheme = AppTheme.values[themeIndex];
          }));

  void _switchTheme() async {
    AppTheme newTheme =
        appTheme == AppTheme.Dark ? AppTheme.Light : AppTheme.Dark;
    setState(() {
      appTheme = newTheme;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('appTheme', newTheme.index);
  }

  void _addFlavor() => setState(() {
        int id = flavors.length == 0 ? 0 : (flavors[flavors.length - 1].id + 1);
        flavors.add(Flavor(name: '', percent: 0.0, id: id));
      });

  void _removeFlavor(BuildContext context, Flavor flavor) {
    int index = flavors.indexOf(flavor);
    setState(() => flavors.remove(flavor));
    Scaffold.of(context).removeCurrentSnackBar();
    Scaffold.of(context).showSnackBar(SnackBar(
      content: flavor.name.trim() != ''
          ? Text('${flavor.name} removed.')
          : Text('Unnamed Flavor removed.'),
      duration: Duration(seconds: 2),
      action: SnackBarAction(
          label: 'Undo',
          onPressed: () => setState(() {
                flavors.insert(index, flavor);
              })),
    ));
  }

  void _clearFlavors() => setState(() {
        flavors = [];
      });

  void _updateFlavorName(Flavor flavor, String newName) => setState(() {
        flavors[flavors.indexOf(flavor)].name = newName;
      });

  void _updateFlavorPercent(Flavor flavor, double newPercent) => setState(() {
        flavors[flavors.indexOf(flavor)].percent = newPercent;
      });

//  void _updateParameter(Parameter parameter, double newValue) {
//    if (parameter == Parameter.Nic ||
//        parameter == Parameter.Target ||
//        parameter == Parameter.Amount) {
//      parameters.update(
//          parameterToKey(parameter), (double oldValue) => newValue);
//    } else if (parameter == Parameter.PG) {
//      parameters.update('pg', (double oldValue) => newValue);
//      parameters.update('vg', (double oldValue) => 100.0 - newValue);
//    } else if (parameter == Parameter.VG) {
//      parameters.update('vg', (double oldValue) => newValue);
//      parameters.update('pg', (double oldValue) => 100.0 - newValue);
//    }
//  }

  void _saveRecipe(String name) => setState(() {
        int id = recipes.length > 0 ? recipes[recipes.length - 1].id + 1 : 0;
        recipes.add(Recipe(name: name, flavors: flavors, id: id));
      });

  Future<void> _loadRecipe(BuildContext outerContext, Recipe recipe) =>
      showDialog<void>(
          context: outerContext,
          barrierDismissible: false,
          builder: (BuildContext innerContext) => AlertDialog(
                title: Text('Load Recipe'),
                content:
                    Text('Would you like to load the recipe ${recipe.name}?'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('No'),
                    onPressed: () {
                      Navigator.of(innerContext).pop();
                    },
                  ),
                  FlatButton(
                    child: Text('Yes'),
                    onPressed: () {
                      setState(() {
                        flavors = recipe.flavors;
                      });
                      Navigator.of(outerContext).pop();
                      Navigator.of(innerContext).pop();
                    },
                  ),
                ],
              ));

  Future<void> _mixRecipe(BuildContext outerContext) => showDialog<void>(
      context: outerContext,
      barrierDismissible: false,
      builder: (BuildContext innerContext) => AlertDialog(
              title: Text('Final Recipe'),
              content: Table(
                  children: flavors
                      .map((Flavor flavor) => TableRow(children: [
                            Text(flavor.name),
                            Text('${flavor.percent.toString()}%')
                          ]))
                      .toList()),
              actions: <Widget>[
                FlatButton(
                    child: Text('k kewl'),
                    onPressed: () => Navigator.of(innerContext).pop())
              ]));

  @override
  Widget build(BuildContext context) => MaterialApp(
      title: 'Mobile Mix',
      theme: appTheme == AppTheme.Dark ? Themes.dark : Themes.light,
      home: Scaffold(
          appBar: AppBar(
            title: Text('Mobile Mix'),
            actions: <Widget>[Menu(switchTheme: _switchTheme)],
          ),
          body: Builder(
              builder: (context) => Center(
                  child: Mixer(
                      flavors: flavors,
                      removeFlavor: _removeFlavor,
                      updateFlavorName: _updateFlavorName,
                      updateFlavorPercent: _updateFlavorPercent))),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () => _addFlavor(),
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          drawer: Drawer(
              child: RecipeDrawer(
                  recipes: recipes,
                  loadRecipe: _loadRecipe,
                  saveRecipe: _saveRecipe)),
          bottomNavigationBar: BottomToolbar(
              mixRecipe: _mixRecipe, clearFlavors: _clearFlavors)));
}
