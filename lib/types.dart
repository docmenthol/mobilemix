class Flavor {
  String name;
  double percent;
  double vg;
  double pg;
  int id;

  Flavor(
      {this.name = '',
      this.percent = 0.0,
      this.vg = 0.0,
      this.pg = 100.0,
      this.id = 0});

  toString() => '$name ($percent%)';
}

class Recipe {
  String name;
  List<Flavor> flavors;
  int id;

  Recipe({this.name, this.flavors, this.id = 0});
}

enum Parameter { Nic, Target, Amount, PG, VG }
enum AppTheme { Dark, Light }
enum MenuAction { about, switchTheme }

String parameterToKey(Parameter p) =>
    p.toString().split('.').last.toLowerCase();
